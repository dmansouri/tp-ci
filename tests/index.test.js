const {
	createDog,
	additionInfo,
	removeEvenNumbers,
	square,
	multiplicationTable,
} = require("../src/index");

describe("As a user", () => {
	describe("I want to breed", () => {
		test("a dog", () => {
			expect(Object.keys(createDog("Leslie", "Poodle", false)).length).toEqual(3);
		});
	});

	describe("I want to get correct data", () => {
		const result = additionInfo(2, -9);
		const expected = {
			x: 2,
			y: -9,
			yAbs: 9,
			sum: -7,
		};
	
		test("on additions", () => {
			expect(result).toMatchObject(expected);
		});
	});

	describe("I want to get an array", () => {
		const evenNumbers = [0, 2, 4, 12];
		const oddNumbers = [1, 3, 11];
		const negativeNumbers = [-1, -4, -13];
		const result = removeEvenNumbers([
			...evenNumbers,
			...oddNumbers,
			...negativeNumbers,
		]);

		test("that doesn't contain even numbers", () => {
			expect(result).not.toEqual(expect.arrayContaining(evenNumbers));
		});

		test("that retain its odd numbers", () => {
			expect(result).toStrictEqual(oddNumbers);
		});

		test("that doesn't contain negative numbers", () => {
			expect(result).not.toEqual(expect.arrayContaining(negativeNumbers));
		});

		test("that is empty when no array is passed as an input", () => {
			expect(removeEvenNumbers()).toEqual([]);
		});
	});

	describe("I want to compute", () => {
		test("the square of an input number", () => {
			expect(square(0)).toEqual(0);
			expect(square(2)).toEqual(4);
			expect(square(5)).toEqual(25);
		});
	});

	describe("I want to compute the multiplication table", () => {
		test("of 0", () => {
			expect(multiplicationTable(0)).toEqual([0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]);
		});

		test("of 5", () => {
			expect(multiplicationTable(5)).toEqual([0, 5, 10, 15, 20, 25, 30, 35, 40, 45, 50]);
		})
	});
});
